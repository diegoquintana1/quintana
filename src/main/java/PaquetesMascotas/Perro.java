/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PaquetesMascotas;

/**
 *
 * @author xxdie
 */
public class Perro {
    private int Edad;
    private String nombre;
    private String alimentacion;
    private String peso;
    private String raza;

    public Perro() {
        
        
        
    }

    public Perro(int Edad, String nombre, String alimentacion, String peso, String raza) {
        this.Edad = Edad;
        this.nombre = nombre;
        this.alimentacion = alimentacion;
        this.peso = peso;
        this.raza = raza;
    }

    
    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAlimentacion() {
        return alimentacion;
    }

    public void setAlimentacion(String alimentacion) {
        this.alimentacion = alimentacion;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
    
    
}
